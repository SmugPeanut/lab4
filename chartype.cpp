#include <iostream>

using namespace std;

int main(){

  char myChar;

  cout << "Enter a character: ";
  cin >> myChar;

  if (myChar >= 65 && myChar <= 122){
    if (myChar == 'A' || myChar == 'E' || myChar == 'I' || myChar == 'O' || myChar == 'U' || myChar == 'a' || myChar == 'e' || myChar == 'i' || myChar == 'o' || myChar == 'u' ){
      cout << "vowel" << endl;
    } else {
      cout << "consonant" << endl;
    }
  } else {
    cout << "Special character" << endl;
  }
}
